import java.awt.AWTException;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.MouseEvent;

//An AutoClicking application I made to automate closing dialog boxes while AFK
public class AutoClicker
{
	private static Robot robot = null;
	public void goRobot() throws InterruptedException
	{
		while(1 == 1)
		{
			try
			{
				robot = new Robot();
			}
			catch(AWTException e)
			{
				e.printStackTrace();
			}
			click(getCenterX(), getCenterY());		//Currently clicks in center of screen with coords returned by getCenterX and getCenterY methods. Pass any coords here if you want instead.
			Thread.sleep(4000);			//Change this value to adjust time interval between clicks
		}
	}
	
	private static void click(int x, int y)
	{
		robot.mouseMove(x, y);
		robot.delay(100);
		robot.mousePress(MouseEvent.BUTTON1_MASK);
		robot.mouseRelease(MouseEvent.BUTTON1_MASK);
	}
	
	private static int getCenterX()
	{
		Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
		return p.x;
	}
	
	private static int getCenterY()
	{
		Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
		return p.y;
	}
}
