import javax.swing.*;

//Simple GUI for AutoClicker. This is the only way to run it.
public class GUI extends JFrame
{
	public static void main(String[] args) throws InterruptedException
	{
	    JFrame frame = new JFrame("AutoClicker");
	    frame.setSize(320,175);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	    JButton button = new JButton("AutoClicker is on. Close this window to stop it.");
	    
	    frame.add(button);

        frame.setVisible(true);
        frame.requestFocus();
	    
	    AutoClicker clicker = new AutoClicker();
	    clicker.goRobot();
	}
}
